runs = 100000000
sum = 0.0

start = Time.now

for i in 1 .. runs do
    sum += rand()
end

end_time = Time.now() - start

puts(end_time * 1000)