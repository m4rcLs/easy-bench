import rand
import time

fn main() {
	runs := 100000000
	mut watch := time.new_stopwatch(time.StopWatchOptions{auto_start: false})
	mut sum := 0.0

	watch.start()

	for i := 0; i < runs; i++ {
		sum += rand.f64()
	}

	watch.stop()
	println(watch.elapsed().milliseconds())

	// This if is needed, so that the compiler doesn't
	// strip out the actual rand loop.
	if sum == 0.0 {
		println (sum)
	}
}
