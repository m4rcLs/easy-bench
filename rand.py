from random import random
import time

runs = 100_000_000
value = 0
start = time.time()
for i in range(runs):
    value += random()

print((time.time() - start) * 1000)
