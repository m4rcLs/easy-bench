# Easy Bench

This is a very simple (pseudo) benchmark of different programming languages.  
Each implementation generates 100.000.000 random floating point numbers (from 0 to 1)  
and adds them up.

This benchmark is not meant to be a extensive comparison in regards to performance.  
It is just meant as a very vague indicator how certain languages and build systems behave.  

I'm also not proficient in any of those languages other than Perl. I tried my best to have  
similar implementations in all languages.

Without further ado, here are the results:

## First Run

```
.-----------------------------------------------------------------------------------------------------------------.
|                                                     Results                                                     |
+----------+-----------------------------------------------------------+--------------+----------------+----------+
| Language | Command                                                   | Compile Time | Execution Time | Total    |
+----------+-----------------------------------------------------------+--------------+----------------+----------+
| Julia    | julia rand.jl                                             |              |   281 ms       |   281 ms |
| Nim      | nim c -d:release -o=./rand_nim rand.nim && ./rand_nim.exe |   703 ms     |   316 ms       |  1019 ms |
| Pypy     | pypy3 rand.py                                             |              |  1200 ms       |  1200 ms |
| Go       | go build -o rand_go.exe && ./rand_go.exe                  |   848 ms     |   435 ms       |  1283 ms |
| V        | v -prod -o rand_v rand.v && ./rand_v.exe                  |  2300 ms     |   654 ms       |  2954 ms |
| Perl     | perl rand.pl                                              |              |  3969 ms       |  3969 ms |
| Rust     | cargo build --release && ./target/release/rand_rust.exe   |  3783 ms     |   664 ms       |  4447 ms |
| Ruby     | ruby rand.rb                                              |              |  6698 ms       |  6698 ms |
| Python   | python rand.py                                            |              | 11048 ms       | 11048 ms |
'----------+-----------------------------------------------------------+--------------+----------------+----------'
```

## Second Run

```
.-----------------------------------------------------------------------------------------------------------------.
|                                                     Results                                                     |
+----------+-----------------------------------------------------------+--------------+----------------+----------+
| Language | Command                                                   | Compile Time | Execution Time | Total    |
+----------+-----------------------------------------------------------+--------------+----------------+----------+
| Julia    | julia rand.jl                                             |              |   287 ms       |   287 ms |
| Go       | go build -o rand_go.exe && ./rand_go.exe                  |   183 ms     |   468 ms       |   651 ms |
| Rust     | cargo build --release && ./target/release/rand_rust.exe   |    73 ms     |   689 ms       |   762 ms |
| Nim      | nim c -d:release -o=./rand_nim rand.nim && ./rand_nim.exe |   676 ms     |   321 ms       |   997 ms |
| Pypy     | pypy3 rand.py                                             |              |  1197 ms       |  1197 ms |
| V        | v -prod -o rand_v rand.v && ./rand_v.exe                  |  2437 ms     |   645 ms       |  3082 ms |
| Perl     | perl rand.pl                                              |              |  4036 ms       |  4036 ms |
| Ruby     | ruby rand.rb                                              |              |  6967 ms       |  6967 ms |
| Python   | python rand.py                                            |              | 10596 ms       | 10596 ms |
'----------+-----------------------------------------------------------+--------------+----------------+----------'
```