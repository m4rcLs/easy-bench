use strict;
use warnings;
use utf8;
use Time::HiRes qw(time);
use Text::ASCIITable;

sub println {
    print "$_[0]\n";
    return;
}

sub pretty_up_time {
    return sprintf("%5d", int($_[0])).' ms';
}
sub get_row {
    my ($language, $values) = @_;
    my $command    =  $values->{compile} ? "$values->{compile} && $values->{run}" : $values->{run};
    my $total_time = $values->{compile_time} ? $values->{compile_time} + $values->{exec_time} : $values->{exec_time};

    return (ucfirst($language),
        $command,
        $values->{compile_time} ?  pretty_up_time($values->{compile_time}) : undef,
        pretty_up_time($values->{exec_time}),
        pretty_up_time($values->{total_time}),
    );
}

my %competitors = (
    perl => {
        run => 'perl rand.pl',
    },
    python => {
        run => 'python rand.py',
    },
    pypy => {
        run => 'pypy3 rand.py',
    },
    julia => {
        run => 'julia rand.jl',
    },
    ruby => {
        run => 'ruby rand.rb',
    },
    go => {
        compile => 'go build -o rand_go.exe',
        run => './rand_go.exe',
    },
    nim => {
        compile => 'nim c -d:release -o=./rand_nim rand.nim',
        run => './rand_nim.exe',
    },
    v => {
        compile => 'v -prod -o rand_v rand.v',
        run => './rand_v.exe',
    },
    rust => {
        compile => 'cargo build --release',
        run => './target/release/rand_rust.exe',
    },
);

my $t = Text::ASCIITable->new({ headingText => 'Results' });

$t->setCols('Language','Command','Compile Time', 'Execution Time', 'Total');

for my $competitor (sort { ord($a) <=> ord($b) } keys %competitors) {
    println ucfirst($competitor);
    my $commands = $competitors{$competitor};

    if (exists $commands->{compile}) {
        my $compile_start = time;
        `$commands->{compile}`;
        $commands->{compile_time} = (time - $compile_start) * 1000;
    }
    my $exec_time = `$commands->{run}`;
    chomp $exec_time;
    $commands->{exec_time} = $exec_time;
    $commands->{total_time} = $commands->{compile_time} ? $commands->{compile_time} + $exec_time : $exec_time;
}

for my $competitor (sort { $competitors{$a}->{total_time} <=> $competitors{$b}->{total_time} } keys %competitors) {
    my $commands = $competitors{$competitor};
    $t->addRow(
        get_row($competitor, $commands)
    );
}


print $t;