extern crate rand;
use rand::prelude::*;
use std::time::Instant;

fn main() {
    let runs = 100000000;
    let mut _sum = 0.0;
    let mut rng = rand::thread_rng();
    let now = Instant::now();
    for _i in 0..runs {
        let random_number: f64 = rng.gen();
        _sum += random_number;
    }
    println!("{}", now.elapsed().as_millis());
}
