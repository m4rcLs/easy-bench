using Dates

# function run ()

function main()
    runs = 100000000
    sum = 0.0
    start = Dates.now()
    for i = 0:runs
        sum += rand(Float64)
    end

    end_time = Dates.now() - start
    println(Dates.value(end_time))
end

main()