use strict;
use warnings;
use utf8;
use Time::HiRes qw(time);

my $runs = 100_000_000;

my $start = time;
my $sum = 0;
for (0..$runs) {
    $sum += rand;
}
my $elapsed = (time - $start) * 1000;
print $elapsed;
