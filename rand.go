package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	r := rand.New(rand.NewSource(99))
	sum := 0.0
	start := time.Now()

	for i := 0; i <= 100000000; i++ {
		sum += r.Float64()
	}

	end := time.Now()
	fmt.Println(end.Sub(start).Milliseconds())
}
