import random, times

randomize()

let runs = 100000000
let start = cpuTime()
var sum = 0.0

for i in 0 .. runs:
  sum += rand(1.0)

echo (cpuTime() - start) * 1000